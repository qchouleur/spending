import * as ko from 'knockout';
import {ComponentRegistry} from 'app/components/componentRegistry';

(function bootstrap()  {
    var registry = new ComponentRegistry();
    registry.register();
    ko.applyBindings({});
})();

