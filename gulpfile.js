var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');
var rimraf = require('rimraf');

var ts = require('gulp-typescript');
var addsrc = require('gulp-add-src');

var tsProject = ts.createProject('tsconfig.json');

var paths = {

    input: {
        scripts: ['app/**/*.ts', 'app/require.config.js'],
        requireConfig: 'app/require.config.js',
        components: 'app/**/*.html'
    },

    output: {
        build: 'dist/app/',
        scripts: 'dist',
        libs: 'dist/libs',
        components: 'dist/app/'
    }
};


gulp.task('scripts', ['require-config'], function () {
    var result = tsProject.src()
        .pipe(ts(tsProject))

    return result.js.pipe(gulp.dest(paths.output.scripts));
});

gulp.task('components', function () {
    return gulp.src(paths.input.components)
        .pipe(gulp.dest(paths.output.components));
});

gulp.task('require-config', function () {
    return gulp.src(paths.input.requireConfig)
        .pipe(gulp.dest(paths.output.scripts));
});

gulp.task('bower', function () {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest(paths.output.libs));
});

gulp.task('build', ['scripts', 'components', 'bower']);

gulp.task('watch', function () {
    gulp.watch(paths.input.scripts, ['build']);
});

gulp.task('clean', function (callback) {
    rimraf(paths.output.build, callback);
});

gulp.task('default', ['build', 'watch']);