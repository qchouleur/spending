import * as ko from 'knockout';

export class ComponentRegistry {

    register() {

        ko.components.register('dashboard',
            {
                viewModel: { require: 'app/components/dashboard/dashboard.viewmodel' },
                template: { require: 'text!app/components/dashboard/dashboard.view.html' }
            });
    }
}